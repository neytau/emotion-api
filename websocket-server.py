#!/usr/bin/env python2

import os
import sys
fileDir = os.path.dirname(os.path.realpath(__file__))

import txaio
txaio.use_twisted()

from autobahn.twisted.websocket import WebSocketServerProtocol, \
    WebSocketServerFactory
from twisted.internet import task, defer
from twisted.internet.ssl import DefaultOpenSSLContextFactory

from twisted.python import log

import argparse
import cv2
import imagehash
import json
from PIL import Image
import numpy as np
import os
import StringIO
import urllib
import base64
import pickle
import keyboard
import pika

from sklearn.decomposition import PCA
from sklearn.grid_search import GridSearchCV
from sklearn.manifold import TSNE
from sklearn.svm import SVC

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import openface
import redis


modelDir = os.path.join(fileDir, 'models')
dlibModelDir = os.path.join(modelDir, 'dlib')
openfaceModelDir = os.path.join(modelDir, 'openface')

tls_crt = os.path.join(fileDir, 'tls', 'server.crt')
tls_key = os.path.join(fileDir, 'tls', 'server.key')

parser = argparse.ArgumentParser()
parser.add_argument('--dlibFacePredictor', type=str, help='Path to dlib\'s face predictor.',
                    default=os.path.join(dlibModelDir, 'shape_predictor_68_face_landmarks.dat'))
parser.add_argument('--networkModel', type=str, help='Path to Torch network model.',
                    default=os.path.join(openfaceModelDir, 'nn4.small2.v1.t7'))
parser.add_argument('--imgDim', type=int,
                    help='Default image dimension.', default=96)
parser.add_argument('--cuda', action='store_true')
parser.add_argument('--unknown', type=bool, default=False,
                    help='Try to predict unknown people')
parser.add_argument('--port', type=int, default=9000,
                    help='WebSocket Port')

args = parser.parse_args()

align = openface.AlignDlib(args.dlibFacePredictor)
net = openface.TorchNeuralNet(args.networkModel, imgDim=args.imgDim, cuda=args.cuda)


class EmotionServer:
    def __init__(self):
        super(OpenFaceServerProtocol, self).__init__()
        fpeople = './sav/fpeople.sav'                                      ######## ajout
        fimages = './sav/fimages.sav'                                      ######## ajout
	       #self.images = {}
        self.images = pickle.load(open(fimages,'rb'))                ######## ajout
        self.training = True
        #self.people = []
        self.people = pickle.load(open(fpeople,'rb'))                ######## ajout
        self.svm = None
        if args.unknown:
            self.unknownImgs = np.load('./examples/web/unknown.npy')


    def redis_connexion(self):                                                                                       ###### ajout
        r = redis.Redis(host='redis-16321.c1.eu-west-1-3.ec2.cloud.redislabs.com', port=16321, password='P@ssw0rd')  ###### ajout
        return r


    def watchQueue():
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.43.20:5672'))
        channel = connection.channel()
        channel.queue_declare(queue='tookPicture')
        def callback(ch, method, properties, body):
            print(body)
            recievePicture(body)
        channel.basic_consume(callback, queue='tookPicture', no_ack=True)
        channel.start_consuming()
                                                                                                           ###### ajout

    def recievePicture(self, payload):
        msg = json.loads(payload)
        self.processFrame(msg['data']['element'], '')


    def processFrame(self, dataURL, identity):
        head = 'data:image/jpeg;base64,'
        assert(dataURL.startswith(head))
        imgdata = base64.b64decode(dataURL[len(head):])
        imgF = StringIO.StringIO()
        imgF.write(imgdata)
        imgF.seek(0)
        img = Image.open(imgF)

        buf = np.fliplr(np.asarray(img))
        rgbFrame = np.zeros((300, 400, 3), dtype=np.uint8)
        rgbFrame[:, :, 0] = buf[:, :, 2]
        rgbFrame[:, :, 1] = buf[:, :, 1]
        rgbFrame[:, :, 2] = buf[:, :, 0]

        if not self.training:
            annotatedFrame = np.copy(buf)

        identities = []

        bb = align.getLargestFaceBoundingBox(rgbFrame)
        bbs = [bb] if bb is not None else []
        for bb in bbs:
            landmarks = align.findLandmarks(rgbFrame, bb)
            alignedFace = align.align(args.imgDim, rgbFrame, bb,
                                      landmarks=landmarks,
                                      landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
            if alignedFace is None:
                continue

            phash = str(imagehash.phash(Image.fromarray(alignedFace)))

            if phash in self.images:
                identity = self.images[phash].identity
            else:
                rep = net.forward(alignedFace)
                if len(self.people) == 0:
                    identity = -1
                elif len(self.people) == 1:
                    identity = 0
                elif self.svm:
                    identity = self.svm.predict(rep)[0]
                else:
                    identity = -1
                if identity not in identities:
                    identities.append(identity)

            if not self.training:
                bl = (bb.left(), bb.bottom())
                tr = (bb.right(), bb.top())
                cv2.rectangle(annotatedFrame, bl, tr, color=(153, 255, 204),
                              thickness=3)
                for p in openface.AlignDlib.OUTER_EYES_AND_NOSE:
                    cv2.circle(annotatedFrame, center=landmarks[p], radius=3,
                               color=(102, 204, 255), thickness=-1)
                if identity == -1:
                    if len(self.people) == 1:
                        name = self.people[0]
                    else:
                        name = 'Unknown'
                else:
                    name = self.people[identity]
                cv2.putText(annotatedFrame, name, (bb.left(), bb.top() - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.75,
                            color=(152, 255, 204), thickness=2)

                r = self.redis_connexion()
                print(name)
                key = 'emotion:'+name
                value = self.timestamp
                r.lpush(key,value)

def main(reactor):
    log.startLogging(sys.stdout)
    emotion = EmotionServer()
    emotion.watchQueue()
    ctx_factory = DefaultOpenSSLContextFactory(tls_key, tls_crt)
    reactor.listenSSL(args.port, factory, ctx_factory)
    return defer.Deferred()

if __name__ == '__main__':
    task.react(main)
